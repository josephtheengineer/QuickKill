﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;
using NHotkey.WindowsForms;

namespace QuickKill
{
    public class Program
    {
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            HotkeyManager.Current.AddOrReplace("KillProcess", Keys.Control | Keys.Alt | Keys.Home, KillProcess);
            Application.Run(new MyCustomApplicationContext());
        }

        public static bool killExplorer = false;

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern Int32 GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        private static Process GetActiveProcess()
        {
            IntPtr hwnd = GetForegroundWindow();
            return hwnd != null ? GetProcessByHandle(hwnd) : null;
        }

        private static Process GetProcessByHandle(IntPtr hwnd)
        {
            try
            {
                uint processID;
                GetWindowThreadProcessId(hwnd, out processID);
                return Process.GetProcessById((int)processID);
            }

            catch { return null; }
        }

        private static void KillProcess(object sender, EventArgs e)
        {
            Process currentProcess = GetActiveProcess();

            string processName = currentProcess.ProcessName;
            if (killExplorer == true)
            {
                processName = "Windows Explorer";
            }

            DialogResult confirmKill = DialogResult.Yes;

            if (Properties.Settings.Default.confirmKill == true)
            {
                confirmKill = MessageBox.Show("Do you wish to kill " + processName + "?", "QuickKill", MessageBoxButtons.YesNo);
            }

            if (killExplorer == true && confirmKill == DialogResult.Yes)
            {
                foreach (var process in Process.GetProcessesByName("explorer"))
                {
                    process.Kill();
                }
                killExplorer = false;
                return;
            }

            else if (confirmKill == DialogResult.Yes)
            {
                currentProcess.Kill();
            }

            else if(confirmKill == DialogResult.No)
            {
                return;
            }

            if (Properties.Settings.Default.showMessage == true)
            {
                NotifyIcon notifyIcon = new NotifyIcon();
                notifyIcon.Visible = true;
                notifyIcon.Icon = Properties.Resources.AppIcon;
                notifyIcon.BalloonTipTitle = "'" + processName + "' killed";
                notifyIcon.BalloonTipText = "QuickKill has killed the process '" + processName + "'.";
                notifyIcon.ShowBalloonTip(5000);
                notifyIcon.Dispose();
            }

            
        }

        private static void explorer(object sender, EventArgs e)
        {
            killExplorer = true;
            KillProcess(killExplorer, null);
        }

        private static void options(object sender, EventArgs e)
        {
            OptionsMenu options = new OptionsMenu();
            options.Show();
        }

        public class MyCustomApplicationContext : ApplicationContext
        {
            private NotifyIcon trayIcon;

            public MyCustomApplicationContext() => trayIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.AppIcon,
                ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Kill the currently running process using CTRL + ALT + HOME"),
                    new MenuItem("Restart Windows Explorer", explorer),
                    new MenuItem("Options", options),
                    new MenuItem("Exit", Exit)
                    }),
                Visible = true
            };

            void Exit(object sender, EventArgs e)
            {
                trayIcon.Visible = false;
                Application.Exit();
            }
        }
    }
}

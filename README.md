# QuickKill

Easy to use program that kills the process of the currently active window by pressing CTRL + ALT + HOME. Works great for games that are frozen or for those who want an easy way to kill programs and are too lazy to open Task Manager each time.
